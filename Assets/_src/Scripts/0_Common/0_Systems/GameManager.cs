using UnityEngine;

public partial class GameManager : MonoBehaviour
{
    public static GameManager _instance;
    public static Pinni Pinni { get; private set; }

    private void Awake()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 120;

        // Singleton pattern
        if (_instance != null)
            Destroy(gameObject);
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }

        SpawnPinni(Vector2.zero);
    }

    private static void SpawnPinni(Vector2 position)
    {
        Pinni = Instantiate(Resources.Load<Pinni>("Characters/Pinni") as Pinni);
        Pinni.Spawn("Pinni", position);
    }
}
