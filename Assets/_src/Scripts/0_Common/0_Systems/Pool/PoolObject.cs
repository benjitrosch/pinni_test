using UnityEngine;

[CreateAssetMenu(fileName = "New Object", menuName = "Pool/PoolObject", order = 1)]
public class PoolObject : ScriptableObject
{
    public GameObject Prefab;

    public string Label;
    public int PoolAmount = 4;
}
    