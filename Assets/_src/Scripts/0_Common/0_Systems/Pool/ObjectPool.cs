using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] private List<PoolObject> _categories;
    private static Dictionary<string, List<GameObject>> _objects;

    private static int _lastForceGrab = 0;

    private void Awake()
    {
        _categories = new List<PoolObject>(Resources.LoadAll<PoolObject>("Pool"));
        _objects = new Dictionary<string, List<GameObject>>();

        foreach (PoolObject item in _categories)
            SpawnObject(item);
    }

    private void SpawnObject(PoolObject item)
    {
        List<GameObject> newObjects = new List<GameObject>();

        for (int i = 0; i < item.PoolAmount; i++)
        {
            GameObject newObject = Instantiate(item.Prefab);

            newObjects.Add(newObject);
            newObject.SetActive(false);
        }

        _objects.Add(item.Label, newObjects);
    }

    public static GameObject GetObject(string category)
    {
        if (!_objects.ContainsKey(category))
        {
            Debug.LogError($"ERROR: Could not find object pool by the name {category}");
            return null;
        }

        List<GameObject> objectPool = new List<GameObject>(_objects[category]);

        for (int i = 0; i < objectPool.Count; i++)
        {
            if (!objectPool[i].activeInHierarchy)
                return objectPool[i];

            if (i == objectPool.Count - 1)
            {
                _lastForceGrab = (_lastForceGrab + 1) % objectPool.Count;
                objectPool[_lastForceGrab].SetActive(false);
                return objectPool[_lastForceGrab];
            }
        }

        return null;
    }
}
