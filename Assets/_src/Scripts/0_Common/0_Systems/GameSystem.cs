using UnityEngine;

[RequireComponent(typeof(DDOL))]
public abstract class GameSystem : MonoBehaviour
{
    public SystemSettings Settings;

    public abstract void OnLoadSystem();

    public void Initialize()
    {
        if (!Settings.Initialized)
        {
            OnLoadSystem();
            Settings.Initialized = true;
        }
    }

    protected void Pause(bool state)
    {
        Settings.Paused = state;
    }
}

public struct SystemSettings
{
    public bool Initialized;
    public bool Paused;
}