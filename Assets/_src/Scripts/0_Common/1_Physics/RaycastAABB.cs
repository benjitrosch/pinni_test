using UnityEngine;

/// <summary>
/// Using raycasts combined with Unity's built in Box Collider 2D, we can create our own pseudo AABB implementation
/// by finding the shape of our collider and casting rays from those points
/// This way we can identify where/when overlap occurs
///
/// Additionally, we can coopt the Rigidbody2D's methods for OnTriggerEnter2D etc. by attaching a Kinematic Rigidbody
/// this way we don't lose ANY functionality, only gain greater modular control
/// </summary>

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public abstract class RaycastAABB : MonoBehaviour
{
	protected Rigidbody2D _rb2d;
	protected BoxCollider2D _box2D;

	[HideInInspector]
	public int HorizontalRayCount, VerticalRayCount;
	[HideInInspector]
	public float HorizontalRaySpacing, VerticalRaySpacing;

	[HideInInspector]
	public RaycastOrigins Origins = new RaycastOrigins();

	protected void UpdateRaycastOrigins()
	{
		// Grab the bounds (the size/shape defined by the vertices) of our collider
		Bounds bounds = _box2D.bounds;
		bounds.Expand(Constants.SKIN_WIDTH * -2);

		// Set the position of our raycast origins to be the corners of our Box2D
		Origins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
		Origins.topCenter = new Vector2((bounds.min.x + bounds.max.x) / 2, bounds.max.y);
		Origins.topRight = new Vector2(bounds.max.x, bounds.max.y);

		Origins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
		Origins.bottomCenter = new Vector2((bounds.min.x + bounds.max.x) / 2, bounds.min.y);
		Origins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);

		Origins.leftCenter = new Vector2(bounds.min.x, (bounds.min.y + bounds.max.y) / 2);
		Origins.centerCenter = new Vector2((bounds.min.x + bounds.max.x) / 2, (bounds.min.y + bounds.max.y) / 2);
		Origins.rightCenter = new Vector2(bounds.max.x, (bounds.min.y + bounds.max.y) / 2);
	}

	protected void CalculateRaySpacing()
	{
		Bounds bounds = _box2D.bounds;
		bounds.Expand(Constants.SKIN_WIDTH * -2);

		float boundsWidth = bounds.size.x;
		float boundsHeight = bounds.size.y;

		// Find out how many raycasts we can make based on the size of the Box2D with how far apart they are
		HorizontalRayCount = Mathf.RoundToInt(boundsHeight / Constants.DISTANCE_BETWEEN_RAYS) * 2;
		VerticalRayCount = Mathf.RoundToInt(boundsWidth / Constants.DISTANCE_BETWEEN_RAYS) * 2;

		HorizontalRaySpacing = bounds.size.y / (HorizontalRayCount - 1);
		VerticalRaySpacing = bounds.size.x / (VerticalRayCount - 1);
	}

	public struct RaycastOrigins
	{
		public Vector2 topLeft, topCenter, topRight;
		public Vector2 bottomLeft, bottomCenter, bottomRight;
		public Vector2 leftCenter, centerCenter, rightCenter;
	}
}
