using UnityEngine;

public abstract class Collision : RaycastAABB
{
	public CollisionInfo collisions;

    protected virtual void Start()
	{
		CalculateRaySpacing();
		collisions.faceDir = 1;
	}

	// Our main function, the entry point to the class. We pass a Vector2 into here, it called every other function we need, and then it moves the entity when all calculations are done.
	public virtual void Move(Vector2 moveAmount)
	{
		// Update where our origins are
		UpdateRaycastOrigins();

		// Reset all new values from the last update so we can write new ones
		collisions.Reset();
		collisions.moveAmountOld = moveAmount;

		if (moveAmount.y < 0)
			DescendSlope(ref moveAmount);

		// If we're moving Horizontally, update which direction (left or right) we're facing.
		// -1 = Left, 1 = Right
		if (moveAmount.x != 0)
			collisions.faceDir = (int)Mathf.Sign(moveAmount.x);

		// Check for collisions against corners to push out of the way
		CornerCorrection(ref moveAmount);

		// Check for Horizontal Collisions
		HorizontalCollisions(ref moveAmount);

		// If we have ANY Vertical movement, check Vertical Collisions (otherwise don't bother)
		if (moveAmount.y != 0)
			VerticalCollisions(ref moveAmount);

		EdgeCollisions(ref moveAmount);

		// Move our transform the most up-to-date moveAmount, which has been changed by ref from all the previous functions
		transform.Translate(moveAmount);

		// We check if we're colliding with ANYTHING for a general all purpose are-we-hitting-anything-or-floating check
		collisions.colliding = collisions.right || collisions.left || collisions.above || collisions.below;
	}

	#region COLLISION DETECTION
	protected virtual void HorizontalCollisions(ref Vector2 moveAmount)
    {
		int directionX = collisions.faceDir;
		float rayLength = Mathf.Abs(moveAmount.x) + Constants.SKIN_WIDTH;

		if (Mathf.Abs(moveAmount.x) < Constants.SKIN_WIDTH)
			rayLength = 2 * Constants.SKIN_WIDTH;

		// Check each raycast for a hit condition
		for (int i = 0; i < HorizontalRayCount; i++)
		{
			Vector2 rayOrigin = (directionX == -1) ? Origins.bottomLeft : Origins.bottomRight;
			rayOrigin += Vector2.up * (HorizontalRaySpacing * i);
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, Constants.LAYER_GROUND);

			if (hit)
			{
				// If we're already inside the collider, it's too late, just return
				if (hit.distance == 0)
					continue;

				// Get the inner angle of the slope by comparing the direction of collision with an upward direction
				float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

				if (i == 0 && slopeAngle <= Constants.SLOPE_ANGLE_MAX)
				{
					if (collisions.descendingSlope)
					{
						collisions.descendingSlope = false;
						moveAmount = collisions.moveAmountOld;
					}
					float distanceToSlopeStart = 0;
					if (slopeAngle != collisions.slopeAngleOld)
					{
						distanceToSlopeStart = hit.distance - Constants.SKIN_WIDTH;
						moveAmount.x -= distanceToSlopeStart * directionX;
					}

					collisions.onSlope = true;
					collisions.slopeSide = -directionX;

					ClimbSlope(ref moveAmount, slopeAngle, hit.normal);
					moveAmount.x += distanceToSlopeStart * directionX;
				}

				if (!collisions.climbingSlope || slopeAngle > Constants.SLOPE_ANGLE_MAX)
				{
					moveAmount.x = (hit.distance - Constants.SKIN_WIDTH) * directionX;
					rayLength = hit.distance;

					if (collisions.climbingSlope)
					{
						moveAmount.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x);
					}

					collisions.left = directionX == -1;
					collisions.right = directionX == 1;
				}
				continue;
			}
		}
	}

	protected virtual void VerticalCollisions(ref Vector2 moveAmount)
    {
		// Find the direction we should check for collisions by finding the sign of our movement
        // -1 = down, 1 = up
		int directionY = (int)Mathf.Sign(moveAmount.y);
		float rayLength = Mathf.Abs(moveAmount.y) + Constants.SKIN_WIDTH;

		// Check each raycast for a hit condition
		for (int i = 0; i < VerticalRayCount; i++)
		{
			Vector2 rayOrigin = (directionY == -1) ? Origins.bottomLeft : Origins.topLeft;
			rayOrigin += Vector2.right * (VerticalRaySpacing * i + moveAmount.x);
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, Constants.LAYER_GROUND);

			if (hit)
			{
				// Move us up (or down) the distance between where we hit and where our raycasts begin
				moveAmount.y = (hit.distance - Constants.SKIN_WIDTH) * directionY;
				rayLength = hit.distance;

				float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

				// If we're on a slope we also need to adjust our X position so we don't slide down
				if (collisions.climbingSlope)
					moveAmount.x = moveAmount.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(moveAmount.x);

				collisions.onSlope = slopeAngle != 0 && slopeAngle != 180;
				collisions.slopeSide = hit.normal.x == 0 ? 0 : (int)Mathf.Sign(hit.normal.x);

				collisions.below = directionY == -1;
				collisions.above = directionY == 1;
				continue;
			}
		}

		if (collisions.climbingSlope)
		{
			int directionX = (int)Mathf.Sign(moveAmount.x);
			rayLength = Mathf.Abs(moveAmount.x) + Constants.SKIN_WIDTH;
			Vector2 rayOrigin = ((directionX == -1) ? Origins.bottomLeft : Origins.bottomRight) + Vector2.up * moveAmount.y;
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, Constants.LAYER_GROUND);

			if (hit)
			{
				float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
				if (slopeAngle != collisions.slopeAngle)
				{
					moveAmount.x = (hit.distance - Constants.SKIN_WIDTH) * directionX;
					collisions.slopeAngle = slopeAngle;
					collisions.slopeNormal = hit.normal;
				}
			}
		}

	}

	void CornerCorrection(ref Vector2 moveAmount)
	{
		int directionY = (int)Mathf.Sign(moveAmount.y);
		float rayLength = Mathf.Abs(moveAmount.y) + Constants.SKIN_WIDTH;

		// We establish a raycast in both top corners and the middle of our entity
		RaycastHit2D hitLeft = Physics2D.Raycast(Origins.topLeft, Vector2.up * directionY, rayLength, Constants.LAYER_GROUND);
		RaycastHit2D hitMid = Physics2D.Raycast(Origins.topCenter, Vector2.up * directionY, rayLength, Constants.LAYER_GROUND);
		RaycastHit2D hitRight = Physics2D.Raycast(Origins.topRight, Vector2.up * directionY, rayLength, Constants.LAYER_GROUND);

		// Find the angles of whatever we're colliding with
		float slopeAngleLeft = Vector2.Angle(hitLeft.normal, Vector2.up);
		float slopeAngleRight = Vector2.Angle(hitLeft.normal, Vector2.up);

		// If both left and right hit, return-- we've just hit a ceiling not a corner
		if ((hitLeft && hitRight))
			return;
		// If just one side hits (and not the middle) then we're on a corner
		else if ((hitLeft && Mathf.Approximately(Mathf.RoundToInt(slopeAngleLeft), 180)
				|| hitRight && Mathf.Approximately(Mathf.RoundToInt(slopeAngleRight), 0))
				&& !hitMid)
        {
			float offset = 0;
			// As long as one side's still colliding we add a pixel to the collision check's origin position
			// Once the origin position + our new offset is no longer touching anything, we exit the while loop
			while (hitLeft || hitRight)
            {
				offset += Constants.CORNER_CORRECTION_PIXELS * (hitLeft ? 1 : -1);

				hitLeft = Physics2D.Raycast(Origins.topLeft + (Vector2.right * offset),
                                Vector2.up * directionY,
                                rayLength,
                                Constants.LAYER_GROUND);
				hitRight = Physics2D.Raycast(Origins.topRight + (Vector2.right * offset),
                                 Vector2.up * directionY,
                                 rayLength,
                                 Constants.LAYER_GROUND);
			}
			// Add the offset we added up to our X movement to push us away from the corner
			moveAmount.x += offset;
		}
	}

	public virtual void EdgeCollisions(ref Vector2 moveAmount)
	{
		float rayLength = .5f;
		// If we're moving left, check left edge, otherwise check right edge
		Vector2 rayOrigin = Mathf.Sign(moveAmount.x) == -1 ? Origins.bottomLeft : Origins.bottomRight;

		RaycastHit2D hitEdge = Physics2D.Raycast(rayOrigin, Vector2.down, rayLength, Constants.LAYER_GROUND);

		if (hitEdge)
		{
			collisions.edge = true;
		}
	}
    #endregion

        #region SLOPES
    protected virtual void ClimbSlope(ref Vector2 moveAmount, float slopeAngle, Vector2 slopeNormal)
	{
		float moveDistance = Mathf.Abs(moveAmount.x);
		float climbmoveAmountY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

		if (moveAmount.y <= climbmoveAmountY)
		{
			moveAmount.y = climbmoveAmountY;
			moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
			collisions.below = true;
			collisions.climbingSlope = true;
			collisions.slopeAngle = slopeAngle;
			collisions.slopeNormal = slopeNormal;
		}
	}

	protected virtual void DescendSlope(ref Vector2 moveAmount)
	{
		RaycastHit2D maxSlopeHitLeft = Physics2D.Raycast(Origins.bottomLeft, Vector2.down, Mathf.Abs(moveAmount.y) + Constants.SKIN_WIDTH, Constants.LAYER_GROUND);
		RaycastHit2D maxSlopeHitRight = Physics2D.Raycast(Origins.bottomRight, Vector2.down, Mathf.Abs(moveAmount.y) + Constants.SKIN_WIDTH, Constants.LAYER_GROUND);
		if (maxSlopeHitLeft ^ maxSlopeHitRight)
		{
			SlideDownMaxSlope(maxSlopeHitLeft, ref moveAmount);
			SlideDownMaxSlope(maxSlopeHitRight, ref moveAmount);
		}

		if (!collisions.slidingDownMaxSlope)
		{
			int directionX = (int)Mathf.Sign(moveAmount.x);
			Vector2 rayOrigin = (directionX == -1) ? Origins.bottomRight : Origins.bottomLeft;
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, -Vector2.up, Mathf.Infinity, Constants.LAYER_GROUND);

			if (hit)
			{
				float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
				if (slopeAngle != 0 && slopeAngle <= Constants.SLOPE_ANGLE_MAX)
				{
					if (Mathf.Sign(hit.normal.x) == directionX)
					{
						if (hit.distance - Constants.SKIN_WIDTH <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x))
						{
							float moveDistance = Mathf.Abs(moveAmount.x);
							float descendmoveAmountY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
							moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
							moveAmount.y -= descendmoveAmountY;

							collisions.slopeAngle = slopeAngle;
							collisions.descendingSlope = true;
							collisions.below = true;
							collisions.slopeNormal = hit.normal;
						}
					}
				}
			}
		}
	}

	protected virtual void SlideDownMaxSlope(RaycastHit2D hit, ref Vector2 moveAmount)
	{
		if (hit)
		{
			float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
			if (slopeAngle > Constants.SLOPE_ANGLE_MAX)
			{
				moveAmount.x = Mathf.Sign(hit.normal.x) * (Mathf.Abs(moveAmount.y) - hit.distance) / Mathf.Tan(slopeAngle * Mathf.Deg2Rad);

				collisions.slopeAngle = slopeAngle;
				collisions.slidingDownMaxSlope = true;
				collisions.slopeNormal = hit.normal;
			}
		}

	}
    #endregion

	// Struct to hold data regarding current collision status, cleared every render
    // (with the exception of cached values)
	public struct CollisionInfo
	{
		public bool above, below;
		public bool left, right;

		public bool colliding, edge;

		public bool onSlope;
		public bool climbingSlope, descendingSlope;
		public bool slidingDownMaxSlope;

		public float slopeAngle, slopeAngleOld;
		public Vector2 slopeNormal;
		public int slopeSide;

		public Vector2 moveAmountOld;
		public int faceDir;

		public void Reset()
		{
			above = below = false;
			left = right = false;

			colliding = false;
			edge = false;

			onSlope = false;
			climbingSlope = false;
			descendingSlope = false;
			slidingDownMaxSlope = false;
			slopeNormal = Vector2.zero;

			slopeAngleOld = slopeAngle;
			slopeAngle = 0;
		}
	}
}
