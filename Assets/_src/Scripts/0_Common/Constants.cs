public static class Constants
{
    #region LAYERS
    // Setting up layers as constant integers and creating masks by bit shifting their position/32 to 1
    private const int _groundLayer = 8;
    private const int _platformLayer = 9;

    private const int _pinniLayer = 10;

    private const int _damageLayer = 20;

    private const int _groundMask = 1 << _groundLayer;
    private const int _platformMask = 1 << _platformLayer;

    public const int LAYER_GROUND = _groundMask | _platformMask;
    public const int LAYER_PINNI = 1 << _pinniLayer;
    public const int LAYER_DAMAGE = 1 << _damageLayer;
    #endregion

    #region PHYSICS
    // Physics system vars
    public const float SKIN_WIDTH = 0.016f;
    public const float DISTANCE_BETWEEN_RAYS = .25f;
    public const float CORNER_CORRECTION_PIXELS = .05f;

    public const int SLOPE_ANGLE_MAX = 80;

    public const float DEFAULT_HITBOX_SCALE = 1f;
    #endregion

    #region MOVEMENT
    public const float DEFAULT_ACCELERATION_TIME = .1f;
    public const float DEFAULT_STOP_TIME = .05f;
    public const float DEFAULT_SPEED = 4.8f;

    public const float DEFAULT_STOP_THRESHOLD = .1f;
    public const float DEFAULT_MOMENTUM_DAMPING = .05f;

    public const float DEFAULT_X_VELOCITY_MIN = -32;
    public const float DEFAULT_X_VELOCITY_MAX = 32;
    public const float DEFAULT_Y_VELOCITY_MIN = -32;
    public const float DEFAULT_Y_VELOCITY_MAX = 32;

    public const float DEFAULT_GRAVITY = -24;

    public const float DEFAULT_JUMP_MAXVELOCITY = 12.8f;
    public const float DEFAULT_JUMP_MINVELOCITY = 6.4f;
    #endregion

    #region ENTITY
    public const int DEFAULT_IFRAMES = 120;
    #endregion

    #region PLAYER
    // Player movement variables (can be moved to player class, but might be good for other objects to have an easy reference)
    public const float PINNI_TIMETO_WALK = .1f;
    public const float PINNI_TIMETO_RUN = .5f;

    public const float PINNI_EDGETOLERANCE_THRESHOLD = .15f;
    public const float PINNI_INPUTBUFFER_THRESHOLD = .15f;

    public const int PINNI_BASE_STAMINA = 4;
    public const int PINNI_BARK_STAMINA_COST = 2;

    public const float PINNI_BARK_FORCE = 8f;
    public const float PINNI_BARK_JUMP_DELAY = 0.4f;
    #endregion

    #region NPC
    public const float DEFAULT_STATE_DELAY = 6.4f;
    public const float DEFAULT_ACTION_DELAY = 2.4f;
    #endregion
}
