using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable] public class AnimationAction : UnityEvent { }

[Serializable]
public abstract class AnimState<A> : ScriptableObject where A : Enum
{
    [Header("Animation Name")]
    [SerializeField] protected A state = default(A);
    [HideInInspector]
    public string AnimationName
    {
        get
        {
            return state.ToString();
        }
    }

    [Space]
    [Header("Properties")]
    public A TransitionState = default(A);
    public AnimationAction Action = null;
}
