using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
public abstract partial class Entity<S, A> : Collision
    where S : System.Enum
    where A : System.Enum
{
    protected virtual void Awake()
    {
        GetComponents();

        // Instantiate all entities as inactive so we can position them/change values before they start
        gameObject.SetActive(false);
    }

    protected virtual void Update()
    {
        UpdateAnimation();

        MoveEntity();

        UpdateMovement();
    }

    private void GetComponents()
    {
        // Grab references to all the components we'll need
        // Collision:
        _rb2d = GetComponent<Rigidbody2D>();
        _box2D = GetComponent<BoxCollider2D>();

        _rb2d.isKinematic = true;

        // Animation:
        _anim = GetComponent<Animator>();
        _sr = GetComponent<SpriteRenderer>();
    }

    #region STATE
    protected Vector2 _startPos;

    protected S _currentState;
    public S CurrentState
    {
        get
        {
            return _currentState;
        }
        private set
        {
            _currentState = value;
        }
    }

    public virtual void Spawn(string name = null, Vector2? position = null)
    {
        // We can put other spawn related calls up here when we override
        // ...

        // If no name is given, use the default name of the prefab-- otherwise, use the passed in name
        if (name != null)
            gameObject.name = name;

        // If no position is given, use the current position-- otherwise, use the passed in position
        if (position != null)
            transform.position = (Vector2)position;

        // Cache spawn position so we can return to/reset if needed
        _startPos = transform.position;

        gameObject.SetActive(true);
    }

    public virtual void Despawn()
    {
        gameObject.SetActive(false);
    }

    public virtual void Reset()
    {
        // We can put reset related calls here (and reset all relevant variables)
        // ...

        transform.position = _startPos;
        gameObject.SetActive(true);
    }

    // Every entity needs some way to manage their internal state
    // so we use the base class's type parameter to create a generic state method
    public virtual void SetState(S state)
    {
        if (_currentState.ToString() == state.ToString() || state.ToString() == "_") return;

        CurrentState = state;
        ConsumeStateValues(_states.Find((s) => s.StateName == state.ToString()));
    }
    #endregion

    #region MOVEMENT
    [Header("States")]
    [SerializeField] protected List<State<S>> _states;

    protected MovementInfo movement;

    protected float _gravity;

    protected Vector2 _velocity;
    protected Vector2 _storedMomentum;

    protected virtual void UpdateMovement()
    {}

    private void MoveEntity()
    {
        CalculateVelocity();
        ResetMomentum();
        ClampVelocity(movement._xVelocityMin,
                      movement._xVelocityMax,
                      movement._yVelocityMin,
                      movement._yVelocityMax);

        // Call Move() from Collision base class
        Move((_velocity + _storedMomentum) * Time.deltaTime);
        Physics2D.SyncTransforms(); // <-- IMPORTANT LINE FOR UNITY TO STOP JITTERING (DUE TO BAD UPDATES)
    }

    protected virtual void ConsumeStateValues(State<S> state)
    {
        // We copy all of the values from the new state into our current member variables
        movement._accelerationTime = state.AccelerationTime;
        movement._stopDampening = state.StopDampening;
        movement._speed = state.Speed;

        movement._xVelocityMin = state.XVelocityMin;
        movement._xVelocityMax = state.XVelocityMax;
        movement._yVelocityMin = state.YVelocityMin;
        movement._yVelocityMax = state.YVelocityMax;

        movement._baseGravity = state.Gravity;
        _gravity = movement._baseGravity;

        movement._jumpVelocityMax = state.JumpVelocityMax;
        movement._jumpVelocityMin = state.JumpVelocityMin;

        movement._hitboxScale = state.HitboxScale;
    }

    protected abstract S CheckStateConditions();

    // Up to each entity to define how they calculate their velocity
    protected abstract void CalculateVelocity();

    protected virtual void ClampVelocity(float minX, float maxX, float minY, float maxY)
    {
        // Sets our maximum speeds in either direction
        _velocity.x = Mathf.Clamp(_velocity.x, minX, maxX);
        _velocity.y = Mathf.Clamp(_velocity.y, minY, maxY);
    }

    // Can be called from outside the entity to add an impulse force (e.g. knockback)
    public virtual void AddForce(Vector2 force)
    {
        _storedMomentum += force;
    }

    private void ResetMomentum()
    {
        // We store an internal momentum in addition to our velocity
        // the momentum gets added to our velocity during movement
        //
        // Because momentum's a separate value we manage it's decceleration separately
        // and constantly bring it towards 0

        Vector2 momentumRef = Vector2.zero;
        _storedMomentum = Vector2.SmoothDamp(_storedMomentum, Vector2.zero, ref momentumRef, Constants.DEFAULT_MOMENTUM_DAMPING);

        if (Mathf.Abs(_storedMomentum.x) < Constants.DEFAULT_STOP_THRESHOLD)
            _storedMomentum = Vector2.zero;
    }
    #endregion

    #region ANIMATION
    [Header("Animations")]
    [SerializeField] protected List<AnimState<A>> _animations;
    
    protected Animator _anim;
    protected SpriteRenderer _sr;

    protected AnimationAction _transitionCallback;

    protected A _transitionState;
    protected A _animState;
    public A AnimState
    {
        get
        {
            return _animState;
        }
        private set
        {
            _animState = value;
        }
    }

    // We cache the previous side so we know whether or not to turn
    protected int _prevSide = 1;
    protected bool _side;
    public bool Side
    {
        get
        {
            return _side;
        }
    }

    protected virtual void UpdateAnimation()
    {
        SetAnimState(CheckAnimationConditions());
    }

    public void AnimationCallback()
    {
        _transitionCallback?.Invoke();
        SetAnimState(_transitionState);
    }

    protected abstract A CheckAnimationConditions();

    protected virtual void ConsumeAnimationValues(AnimState<A> state)
    {
        _transitionState = state.TransitionState;
        _transitionCallback = state.Action;
    }


    // Same as regular state but using a second generic parameter
    public virtual void SetAnimState(A state)
    {
        if (_animState.ToString() == state.ToString() || state.ToString() == "_") return;

        // Get animation clip value from enum's value as a string
        AnimState = state;
        ConsumeAnimationValues(_animations.Find((a) => a.AnimationName == state.ToString()));
        _anim.Play(AnimState.ToString());
    }
    #endregion

    #region DAMAGE
    private bool _damageable = true;

    public bool CheckHitDamage(int layer)
    {
        return Physics2D.OverlapBox((Vector2)transform.position + (_box2D.offset),
                                    _box2D.size * movement._hitboxScale,
                                    0,
                                    layer);
    }

    protected void Damage(Action onDamage, Action callback = null, int iFrames = Constants.DEFAULT_IFRAMES)
    {
        if (!_damageable) return;

        _damageable = false;
        new InvokeAfterFrames(this, () => {
            _damageable = true;
            callback?.Invoke();
            }, iFrames);

        onDamage?.Invoke();
    }
    protected void Damage(Action onDamage, int iFrames = Constants.DEFAULT_IFRAMES) // Method overload for case of no callback delegate
    {
        Damage(onDamage, null, iFrames);
    }
    protected void Damage(Action onDamage)
    {
        Damage(onDamage, null, Constants.DEFAULT_IFRAMES);
    }
    #endregion

    protected virtual void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        // Draw hitbox
        if (_box2D != null)
        {
            Gizmos.DrawWireCube((Vector2)transform.position + (_box2D.offset),
                    _box2D.size * movement._hitboxScale);
        }
    }
}

// Struct to hold all of the entities base movement stats
// Only changed/hydrated on a SetState call
// 
// important: these are VALUES, not REFERENCES
// Do not use/attempt to change outside context of entity state
public struct MovementInfo
{
    public float _accelerationTime;
    public float _stopDampening;
    public float _speed;

    public float _xVelocityMin, _xVelocityMax;
    public float _yVelocityMin, _yVelocityMax;

    public float _baseGravity;

    public float _jumpVelocityMax;
    public float _jumpVelocityMin;

    public float _hitboxScale;
}