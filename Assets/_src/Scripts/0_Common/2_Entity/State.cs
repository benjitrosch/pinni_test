using System;
using UnityEngine;

[Serializable]
public abstract class State<S> : ScriptableObject where S : Enum
{
    [Header("State Name")]
    [SerializeField] protected S state;
    [HideInInspector]
    public string StateName
    {
        get
        {
            return state.ToString();
        }
    }

    [Space]
    [Header("Movement Values")]
    public float AccelerationTime = Constants.DEFAULT_ACCELERATION_TIME;
    public float StopDampening = Constants.DEFAULT_STOP_TIME;
    public float Speed = Constants.DEFAULT_SPEED;

    [Space]
    [Header("Clamp")]
    public float XVelocityMin = Constants.DEFAULT_X_VELOCITY_MIN;
    public float XVelocityMax = Constants.DEFAULT_X_VELOCITY_MAX;
    [Space]
    public float YVelocityMin = Constants.DEFAULT_Y_VELOCITY_MIN;
    public float YVelocityMax = Constants.DEFAULT_Y_VELOCITY_MAX;

    [Space]
    [Header("Jump Values")]
    public float Gravity = Constants.DEFAULT_GRAVITY;

    [Space]
    public float JumpVelocityMax = Constants.DEFAULT_JUMP_MAXVELOCITY;
    public float JumpVelocityMin = Constants.DEFAULT_JUMP_MINVELOCITY;

    [Space]
    [Header("Collision")]
    public float HitboxScale = Constants.DEFAULT_HITBOX_SCALE;
}
