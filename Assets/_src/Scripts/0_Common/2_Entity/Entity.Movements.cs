using UnityEngine;

/// <summary>
/// We can populate this script with template movements that NPC's can implement
/// Basic movements like walking or flying can be re-used and even combined
/// 
/// Each method should take a direction (for pinni it will be input) and a ref to velocity
/// </summary>

public partial class Entity<S, A>
{
    protected virtual void Walk(Vector2 direction, ref Vector2 velocity)
    {
        // Find out what state we should be in so we know what to calculate
        SetState(CheckStateConditions());

        float velocityXDamping = 0;
        float targetVelocityX = direction.x * movement._speed;

        // Ease between current velocity value and our target
        // Use our acceleration time if we have an input, and our stop dampening if we don't
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXDamping, direction.x == 0 ? movement._stopDampening : movement._accelerationTime);
        velocity.y += (_gravity * Time.deltaTime);

        // If our velocity X is close to zero and we have no input, reset to zero
        if (Mathf.Abs(velocity.x) < Constants.DEFAULT_STOP_THRESHOLD && direction.x == 0)
            velocity.x = 0;
    }
}
