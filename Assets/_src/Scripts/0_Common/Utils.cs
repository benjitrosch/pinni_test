using System;
using System.Collections;
using UnityEngine;

public class InvokeAfterFrames : CustomYieldInstruction
{
    private readonly int m_TargetFrameCount;

    public InvokeAfterFrames(MonoBehaviour context, Action callback, int numberOfFrames = 1)
    {
        m_TargetFrameCount = Time.frameCount + numberOfFrames;
        context.StartCoroutine(Coroutine(callback));
    }

    public override bool keepWaiting => Time.frameCount < m_TargetFrameCount;

    private IEnumerator Coroutine(Action callback)
    {
        yield return this;
        callback.Invoke();
    }
}

public class AnimationUtils
{
    public static AnimationClip FindAnimation(Animator animator, string name)
    {
        foreach (AnimationClip clip in animator.runtimeAnimatorController.animationClips)
        {
            if (clip.name == name)
            {
                return clip;
            }
        }

        return null;
    }
}