public enum NPCAnimStates
{
    _,

    IDLE,
    WALK,

    SHOOT,

    HURT,
}