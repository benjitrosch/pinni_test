using UnityEngine;

[CreateAssetMenu(fileName = "New NPC State", menuName = "Entity/NPC/State", order = 1)]
public class NPCState : State<NPCStates>
{
    [Space]
    [Header("Delay/Timing")]
    public float StateDelay = Constants.DEFAULT_STATE_DELAY;
    public float ActionDelay = Constants.DEFAULT_ACTION_DELAY;
}
