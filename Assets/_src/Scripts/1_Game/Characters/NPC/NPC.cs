using UnityEngine;

public abstract partial class NPC<S, A> : Entity<S, A>
    where S : System.Enum
    where A : System.Enum
{
    [Space]
    [Header("Start Movement Direction")]
    public Vector2 Direction;

    protected float _stateDelay;
    protected float _actionDelay;

    [Space]
    [Header("Aggro Distance")]
    [SerializeField] private float AggroDistance;
    [SerializeField] private Vector2 AggroOffset;

    public bool CheckPlayerAggro()
    {
        // Raycast a circle with a radius of our aggro distance to check for the player
        return Physics2D.OverlapCircle((Vector2)transform.position + (AggroOffset * Direction), AggroDistance, Constants.LAYER_PINNI);
    }

    protected bool CheckWalls()
    {
        return collisions.left || collisions.right;
    }

    protected bool CheckEdges()
    {
        // If our edge check raycast isn't true (aka no collider on corner) but still hitting ground, we're on an edge
        return !collisions.edge && collisions.below;
    }

    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere((Vector2)transform.position + (AggroOffset * Direction), AggroDistance);
    }
}
