using UnityEngine;

[CreateAssetMenu(fileName = "New NPC Animation", menuName = "Entity/NPC/Animation", order = 2)]
public class NPCAnimation : AnimState<NPCAnimStates>
{ }
