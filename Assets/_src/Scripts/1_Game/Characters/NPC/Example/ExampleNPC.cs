using UnityEngine;

public class ExampleNPC : NPC<NPCStates, NPCAnimStates>
{
    protected override void Start()
    {
        base.Start();

        SetState(NPCStates.PATROL);
        SetAnimState(NPCAnimStates.WALK);
    }

    protected override void CalculateVelocity()
    {
        // If we hit a wall or the edge of a platform, turn around
        if (CheckWalls() || CheckEdges())
        {
            _velocity.x = -_velocity.x;
            Direction.x = -Direction.x;
        }

        Walk(Direction, ref _velocity);
    }

    protected override NPCStates CheckStateConditions()
    {
        if (CheckPlayerAggro()) return NPCStates.ATTACK;
        else return NPCStates.PATROL;
    }

    protected override NPCAnimStates CheckAnimationConditions()
    {
        if (_currentState == NPCStates.ATTACK) return NPCAnimStates.SHOOT;
        else return NPCAnimStates.WALK;
    }
}
