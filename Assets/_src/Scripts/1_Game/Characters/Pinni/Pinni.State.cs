using UnityEngine;

/// <summary>
/// These represent movement states and can be used to conditionally alter the values used during velocity calculation
/// 
/// An example of what wouldn't a state is "IDLE," because being idle lacks movement and doesn't require any changes
/// An example of what could be a state is "SWIMMING," because underwater movement would need completely different values to produce a floating effect
/// </summary>
public enum PlayerState
{
    _,

    WALK,
    RUN,

    HURT,
}

public partial class Pinni
{
    private void StartState()
    {
        SetState(PlayerState.WALK);
    }
}
