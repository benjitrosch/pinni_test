using UnityEngine;

/// <summary>
/// Animations are represented by enum states whose toString() value can be converted to the name of an animation clip inside the animator
/// Every time we change state, we grab the string value of the new state and play that clip
/// 
/// We determine which animation to play (and the order of priority) by cascading if statements, to prevent any conflict/conditions that might apply to multiple animation states
/// Maintainability is key and therefore simple conditions and cleanliness of code should be a priority for the update function
/// </summary>

// Name all animations in ALL CAPS exactly as they are in the enum
public enum PlayerAnimState
{
    // Init (should never be this state except on initialization, and should be changed on start)
    _,

    // No movement:
    IDLE,
    LAND,
    STOP,

    // Horizontal movement:
    WALK,
    RUN,
    TURN,
    PUSH,
    LAND_TO_RUN,

    // Vertical movement:
    JUMP,
    FALL,
    TURN_MIDAIR,

    // Bark:
    BARK,
    BARK_JUMP,

    HURT,
}

public partial class Pinni
{
    private void StartAnimation()
    {
        SetAnimState(PlayerAnimState.IDLE);
    }

    protected override void UpdateAnimation()
    {
        base.UpdateAnimation();

        if (!_barking)
            SetFlip();
    }

    protected override PlayerAnimState CheckAnimationConditions()
    {
        // To make our animation conditions easy we split them between on the ground vs. in the air
        switch (_groundTouch)
        {
            case true:
                if (_barking) return PlayerAnimState.BARK;
                else if (_currentState == PlayerState.HURT) return PlayerAnimState.HURT;
                else if ((Mathf.Sign(_input.x) == 1 && collisions.right) || (Mathf.Sign(_input.x) == -1 && collisions.left)) return PlayerAnimState.PUSH;
                else if (_input.x == 0)
                {
                    if (_animState == PlayerAnimState.LAND) return PlayerAnimState.LAND;
                    else if (_animState == PlayerAnimState.RUN || _animState == PlayerAnimState.STOP) return PlayerAnimState.STOP;
                    else if (_velocity.x == 0) return PlayerAnimState.IDLE;
                }
                else if (_currentState == PlayerState.WALK) return PlayerAnimState.WALK;
                else if (_currentState == PlayerState.RUN)
                {
                    if (_animState == PlayerAnimState.LAND_TO_RUN) return PlayerAnimState.LAND_TO_RUN;
                    else if (_animState == PlayerAnimState.TURN) return PlayerAnimState.TURN;
                    else if (_animState == PlayerAnimState.WALK) return PlayerAnimState.WALK;
                    else return PlayerAnimState.RUN;
                }
                break;

            case false:
                if (_barking) return PlayerAnimState.BARK_JUMP;
                else if (_velocity.y > 0) return PlayerAnimState.JUMP;
                else if (_velocity.y <= 0) return PlayerAnimState.FALL;
                break;
        }

        return PlayerAnimState._;
    }

    private void Land()
    {
        SetAnimState(_currentState == PlayerState.RUN ? PlayerAnimState.LAND_TO_RUN : PlayerAnimState.LAND);
    }

    #region SIDE
    private void SetFlip()
    {
        int directionX = (int)Mathf.Sign(_velocity.x);
        if (_velocity.x != 0) _side = directionX != 1;

        // Check if the side has changed, and if so, turn
        if (directionX != _prevSide)
        {
            _prevSide = directionX;
            if (_groundTouch && _currentState == PlayerState.RUN)
            {
                SetAnimState(PlayerAnimState.TURN);
            }
        }

        _sr.flipX = _side;
    }
    #endregion
}
