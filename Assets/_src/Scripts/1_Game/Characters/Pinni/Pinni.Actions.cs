using System.Collections;
using UnityEngine;

public partial class Pinni
{
    private int _stamina;
    [HideInInspector] public int Stamina
    {
        get
        {
            return _stamina;
        }
        set
        {
            _stamina = value;
        }
    }

    private bool _barking;
    [HideInInspector] public bool Barking
    {
        get
        {
            return _barking;
        }
        set
        {
            _barking = value;
        }
    }

    private void StartActions()
    {
        ResetStamina();
    }

    public void ResetStamina()
    {
        _stamina = Constants.PINNI_BASE_STAMINA;
    }

    public void Bark(Vector2 direction)
    {
        if (!_groundTouch)
        {
            CallJump(true);
            StartCoroutine(DisableInput(Constants.PINNI_BARK_JUMP_DELAY));
        }

        _stamina -= Constants.PINNI_BARK_STAMINA_COST;
        _barking = true;
    }

    public void EndBark()
    {
        GameManager.Pinni.Barking = false;
    }

    public void EndHurt()
    {
        GameManager.Pinni.SetState(PlayerState.RUN);
    }
}
