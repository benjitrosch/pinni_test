public partial class Pinni : Entity<PlayerState, PlayerAnimState>
{
    private Pinni _instance;

    protected override void Awake()
    {
        base.Awake();

        // Singleton pattern because there should only ever be one player
        if (_instance != null) Destroy(gameObject);
        else _instance = this;

        // We can put any Awake functions from our partial files here to be called on Awake
        // ...
    }

    protected override void Start()
    {
        base.Start();

        // We can put any Start functions from our partial files here to be called on Start
        // ...

        StartState();
        StartMovement();
        StartAnimation();
        StartActions();
    }

    protected override void Update()
    {
        // We can put any Update functions from our partial files here to be called on Update
        // ...
        UpdateInput();

        // This is where Pinni gets moved
        base.Update();
    }
}
