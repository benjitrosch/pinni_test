using UnityEngine;

[CreateAssetMenu(fileName = "New Pinni Animation", menuName = "Entity/Pinni/Animation", order = 2)]
public class PinniAnimation : AnimState<PlayerAnimState>
{}
