using System.Collections;
using UnityEngine;

public partial class Pinni
{
    private Vector2 _input;
	private float _inputBuffer;

	private bool _jumpKeyDown;
	private bool _inputDisabled;

    private void UpdateInput()
    {
		if (_inputDisabled) return;

        _input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if (Input.GetButtonDown("Jump"))
            jumpInputDown();

        if (Input.GetButton("Jump"))
            OnJumpHold();

        if (Input.GetButtonUp("Jump"))
            jumpInputUp();

		if (Input.GetButtonDown("Fire2") && _stamina >= Constants.PINNI_BARK_STAMINA_COST)
			Bark(_input);
    }

	private void jumpInputDown()
	{
		_jumpKeyDown = true;

		OnJumpInputDown();

		// Reset the input buffer whenever we call our jump input
		StopCoroutine("JumpBuffer");
		_inputBuffer = 0;
		StartCoroutine("JumpBuffer");
	}

	private void jumpInputUp()
    {
		_jumpKeyDown = false;

		OnJumpInputUp();
	}

	private IEnumerator JumpBuffer()
	{
		while (true)
		{
			_inputBuffer += Time.deltaTime;
			yield return null;
		}
	}

	private IEnumerator DisableInput(float time = .1f)
    {
		_inputDisabled = true;
		yield return new WaitForSeconds(time);

		_inputDisabled = false;
		yield break;
    }
}
