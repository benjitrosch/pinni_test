using UnityEngine;

[CreateAssetMenu(fileName = "New Pinni State", menuName = "Entity/Pinni/State", order = 1)]
public class PinniState : State<PlayerState>
{}
