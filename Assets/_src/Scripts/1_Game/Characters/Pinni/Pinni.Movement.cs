using UnityEngine;

/// <summary>
/// Every method within the movement partial should serve the purpose of altering velocity
/// As long as the outcome of a method follows this goal the outcome will work seamlessly with the physics system
///
/// Establishing a consistent pattern between movement functions will guarantee that new methods work as intended
/// </summary>

public partial class Pinni
{
    private float _walkTime;
    private float _runTime;

    private float _edgeTolerance;

    private bool _groundTouch;
    private bool _jumpHasBeenCalled;

    private void StartMovement()
    {}

    protected override void UpdateMovement()
    {
        base.UpdateMovement();
        CheckCollision();
    }

    #region COLLISION
    private void CheckCollision()
    {
        #region GROUND
        // If we're touching the ground or a roof, check if we're sliding down a slope. If not, set our Y velocity to 0 so we stop falling/going up.
        if (collisions.above || collisions.below)
        {
            if (collisions.slidingDownMaxSlope) _velocity.y += collisions.slopeNormal.y * _gravity * Time.deltaTime;
            else _velocity.y = 0;
        }

        // If we're pushing into a wall, stop our X velocity
        if (_input.x != 0 && ((Mathf.Sign(_velocity.x) == 1 && collisions.right) || (Mathf.Sign(_velocity.x) == -1 && collisions.left)))
        {
            _velocity.x = 0;
        }

        if (!collisions.below)
        {
            // As long as we're not touching ground increase our edge tolerance value
            if (_edgeTolerance <= Constants.PINNI_EDGETOLERANCE_THRESHOLD) _edgeTolerance += Time.deltaTime;
            if (_groundTouch) _groundTouch = false;
        }

        if (collisions.below && !_groundTouch)
            GroundTouch();
        #endregion

        #region ENEMY
        if (CheckHitDamage(Constants.LAYER_DAMAGE))
            Damage(() =>
            {
                SetState(PlayerState.HURT);
                _sr.color = Color.red;
            },
            () => _sr.color = Color.white); // TEMP: change sprite color to red to see iFrames duration
        #endregion
    }

    private void GroundTouch()
    {
        _groundTouch = true;
        _jumpHasBeenCalled = false;

        _gravity = movement._baseGravity;
        _edgeTolerance = 0;

        // If we hit the ground and our input buffer is within an acceptable threshold, we make the player jump to compensate for the early input
        if (_inputBuffer > Constants.PINNI_INPUTBUFFER_THRESHOLD)
        {
            StopCoroutine("JumpBuffer");
            _inputBuffer = 0f;
        }
        else if (_inputBuffer > 0 && _inputBuffer <= Constants.PINNI_INPUTBUFFER_THRESHOLD)
            CallJump();

        Land();
        ResetStamina();
    }
    #endregion

    #region VELOCITY
    protected override void CalculateVelocity()
    {
        Walk(_input, ref _velocity);
    }
#endregion

    #region JUMP
    public void OnJumpInputDown()
    {
        // If we're colliding with the roof above us or we've already gotten a free jump from the input buffer, don't let us jump
        if (collisions.above || _jumpHasBeenCalled) return;

        // If we're on the ground OR we're off the ground but within an acceptable time off the ledge (coyote time) we're allowed to jump
        if (collisions.below || _edgeTolerance < Constants.PINNI_EDGETOLERANCE_THRESHOLD)
        {
            // Set edge tolerance to the threshold so that we can't jump again
            _edgeTolerance = Constants.PINNI_EDGETOLERANCE_THRESHOLD;
            _jumpHasBeenCalled = true;

            if (collisions.slidingDownMaxSlope)
            {
                if (_input.x != -Mathf.Sign(collisions.slopeNormal.x))
                {
                    _velocity.y = movement._jumpVelocityMax * collisions.slopeNormal.y;
                    _velocity.x = movement._jumpVelocityMax * collisions.slopeNormal.x;
                }
            }
            else _velocity.y = movement._jumpVelocityMax;
        }
    }

    public void OnJumpHold()
    {
        // When we reach the apex of the jump and are still holding jump, gravity becomes 3/4 so they fall slower
        if (_velocity.y < 0)
            _gravity = movement._baseGravity * .75f;
        else
            _gravity = movement._baseGravity;
    }

    public void OnJumpInputUp()
    {
        // When we let go of jump, set our jump velocity to it's lowest so we can slow down quicker
        if (_velocity.y > movement._jumpVelocityMin)
            _velocity.y = movement._jumpVelocityMin;
    }

    public void CallJump(bool ignore = false)
    {
        if (_jumpHasBeenCalled && !ignore)
            return;

        _jumpHasBeenCalled = true;

        // Since the jump input's recorded we might miss the keyup event so we compare against if it's currently down
        _velocity.y = _jumpKeyDown || ignore ?
            movement._jumpVelocityMax :
            movement._jumpVelocityMin;
    }
    #endregion

    #region STATE
    // This method's called to figure out which state we're before moving
    // It runs once every frame, so any states that need to be persistent should return themselves here
    protected override PlayerState CheckStateConditions()
    {
        if (_currentState == PlayerState.HURT) return PlayerState.HURT;

        // We're holding down any X direction while touching the ground, increase runTime
        if (Mathf.Abs(_input.x) > 0 && _groundTouch)
        {
            _walkTime = 0;
            _runTime += Time.deltaTime;
        }
        else if (_input.x == 0)
        {
            _walkTime += Time.deltaTime;
            _runTime = 0;
        }

        // When our runtime goes past our time to run, set our state to running
        // Otherwise, return to walking
        if (_runTime >= Constants.PINNI_TIMETO_RUN) return PlayerState.RUN;
        if (_walkTime >= Constants.PINNI_TIMETO_WALK) return PlayerState.WALK;

        return PlayerState._;
    }
    #endregion
}
